public class MethodsTest{
  public static void main(String[]args){ 
    int x = 10;
    System.out.println(x);
    methodNoInputNoReturn();
	System.out.println(x);
	methodOneInputNoReturn(10);
	methodOneInputNoReturn(x);
	methodOneInputNoReturn(x+50);
	methodTwoInputNoReturn(10, 5.5);
	int z = methodNoInputReturnInt();
	System.out.println(z);
	double s = sumSquareRoot(6, 3);
	System.out.println(s);
	String s1 = "hello";
	String s2 = "goodbye";
	System.out.println(s1.length());
	System.out.println(s2.length());
	System.out.println(SecondClass.addOne(50));
	SecondClass sc = new SecondClass();
	System.out.println(sc.addTwo(50));
  }
  
  public static void methodNoInputNoReturn(){
    int x = 50;
    System.out.println("I�m in a method that takes no input and returns nothing");
    System.out.println(x);
  }
  
  public static void methodOneInputNoReturn(int x){
	  System.out.println("Inside the method one input no return");
	  System.out.println(x);
  }
  
  public static void methodTwoInputNoReturn(int x, double y){
	  System.out.println("Inside the method two input no return");
	  System.out.println(x);
	  System.out.println(y);
  }
  
  public static int methodNoInputReturnInt(){
	  
	  return 6;
  }
  
  public static double sumSquareRoot(int x, int y){
	  double z = x + y;
	  z = Math.sqrt(z);
	  System.out.println("Inside the method sum square root");
	  return z;
  }
}