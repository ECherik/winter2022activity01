import java.util.Scanner;
public class PartThree{
	public static void main(String[]args){
		Scanner reader = new Scanner(System.in);
		AreaComputations ac = new AreaComputations();
		
		System.out.println("Please enter the length of the square:");
		int lengthSquare = reader.nextInt();
		System.out.println("Please enter the length of the Rectangle:");
		int lengthRectangle = reader.nextInt();
		System.out.println("Please enter the width of the Rectangle:");
		int widthRectangle = reader.nextInt();
		
		int squareArea = AreaComputations.areaSquare(lengthSquare);
		int rectangleArea = ac.areaRectangle(lengthRectangle, widthRectangle);
		
		System.out.println("The area of the square is: " + squareArea);
		System.out.println("The area of the rectangle is: " + rectangleArea);
	}

}